from random import randint
from typing import List, Optional
import boto3
import uuid
from fastapi import FastAPI
from concurrent.futures import ThreadPoolExecutor
import time

# Aquí debes agregar tus propias credenciales y URL de cola
aws_access_key_id = 'TU_ACCESS_KEY_ID'
aws_secret_access_key = 'TU_SECRET_ACCESS_KEY'
queue_url = 'TU_URL_DE_COLA'
bucket_name = 'ttransbank'

sqs = boto3.client(
    'sqs',
    region_name='us-east-1',
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key
)

s3 = boto3.client(
    's3',
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key
)

app = FastAPI()

def encolar(cantidad_mensajes: int):
    mensajes = []
    for i in range(cantidad_mensajes):
        response = sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=f"{randint(0, cantidad_mensajes)}",
            MessageGroupId='pagos',
            MessageDeduplicationId=str(uuid.uuid4())
        )
        mensajes.append(response['MessageId'])
        print(f"Enviando Transaction {i} - {response['MessageId']}")
    return mensajes

def process_message(message):
    print(f"Mensaje recibido: {message['Body']}")
    sqs.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=message['ReceiptHandle']
    )
    return int(message['Body'])

def obtener_tamano_cola():
    response = sqs.get_queue_attributes(
        QueueUrl=queue_url,
        AttributeNames=['ApproximateNumberOfMessages']
    )
    return int(response['Attributes']['ApproximateNumberOfMessages'])

def desencolar():
    mensajes_desencolados = []
    while True:
        response = sqs.receive_message(
            QueueUrl=queue_url,
            AttributeNames=['All'],
            MessageAttributeNames=['All'],
            MaxNumberOfMessages=5,
            VisibilityTimeout=30,
            WaitTimeSeconds=0
        )

        if 'Messages' in response:
            messages = response['Messages']
            print(f"Número de mensajes recibidos: {len(messages)}")

            with ThreadPoolExecutor() as executor:
                results = list(executor.map(process_message, messages))
                mensajes_desencolados.extend(results)
        else:
            print("No se encontraron más mensajes en la cola.")
            break

        # Agregar una pausa de 1 segundo entre las solicitudes de mensajes
        time.sleep(1)

    # Obtener el tamaño de la cola después de desencolar todos los mensajes
    tamano_cola = obtener_tamano_cola()

    return mensajes_desencolados, tamano_cola


class AVLNode:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None
        self.height = 1

class AVLTree:
    def insert(self, root: Optional[AVLNode], key: int) -> AVLNode:
        if not root:
            return AVLNode(key)
        elif key < root.key:
            root.left = self.insert(root.left, key)
        elif key > root.key:
            root.right = self.insert(root.right, key)
        else:
            return root

        root.height = 1 + max(self.get_height(root.left), self.get_height(root.right))

        balance = self.get_balance(root)

        if balance > 1 and key < root.left.key:
            return self.right_rotate(root)
        if balance < -1 and key > root.right.key:
            return self.left_rotate(root)
        if balance > 1 and key > root.left.key:
            root.left = self.left_rotate(root.left)
            return self.right_rotate(root)
        if balance < -1 and key < root.right.key:
            root.right = self.right_rotate(root.right)
            return self.left_rotate(root)

        return root

    def left_rotate(self, z: AVLNode) -> AVLNode:
        y = z.right
        T2 = y.left

        y.left = z
        z.right = T2

        z.height = 1 + max(self.get_height(z.left), self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left), self.get_height(y.right))

        return y

    def right_rotate(self, z: AVLNode) -> AVLNode:
        y = z.left
        T3 = y.right

        y.right = z
        z.left = T3

        z.height = 1 + max(self.get_height(z.left), self.get_height(z.right))
        y.height = 1 + max(self.get_height(y.left), self.get_height(y.right))

        return y

    def get_height(self, root: Optional[AVLNode]) -> int:
        if not root:
            return 0
        return root.height

    def get_balance(self, root: Optional[AVLNode]) -> int:
        if not root:
            return 0
        return self.get_height(root.left) - self.get_height(root.right)

    def pre_order(self, root: Optional[AVLNode]):
        res = []
        if root:
            res.append(root.key)
            res = res + self.pre_order(root.left)
            res = res + self.pre_order(root.right)
        return res

def crear_arbol_binario(mensajes: List[int]):
    arbol = AVLTree()
    root = None
    for mensaje in mensajes:
        root = arbol.insert(root, mensaje)
    return root, arbol

def graficar_arbol(root: Optional[AVLNode], level=0, pref="Root:"):
    if root is not None:
        print(" " * (level * 4) + pref + str(root.key))
        if root.left:
            graficar_arbol(root.left, level + 1, "L---")
        if root.right:
            graficar_arbol(root.right, level + 1, "R---")

@app.post("/encolar")
def sqs_encolar(parametros: dict):
    mensajes = encolar(parametros["cantidad_mensajes"])
    return {
        "mensajes_encolados": mensajes,
        "cantidad_encolada": len(mensajes)
    }

@app.post("/desencolar")
def sqs_desencolar(parametros: dict):
    mensajes, tamano_cola = desencolar()

    root, arbol = crear_arbol_binario(mensajes)
    print("Árbol AVL generado (Pre-Order):", arbol.pre_order(root))
    graficar_arbol(root)

    return {
        "desencolados": mensajes,
        "cantidad_desencolados": len(mensajes),
        "tamano_cola_actualizado": tamano_cola,
        "arbol_generado": arbol.pre_order(root)
    }


@app.post("/consumir")
def sqs_consumir():
    tamano_cola = obtener_tamano_cola()
    mensajes = desencolar()

@app.post("/consumir")
def sqs_consumir():
    tamano_cola = obtener_tamano_cola()
    mensajes = desencolar()

    def procesar_archivo(mensaje):
        archivo = f"Archivos/{mensaje}.txt"
        try:
            print(f"Intentando obtener el archivo: {archivo}")
            response = s3.get_object(Bucket='ttransbank', Key=archivo)
            contenido = response['Body'].read().decode('utf-8')
            print(f"Procesando archivo {archivo} con contenido: {contenido}")

            nuevo_contenido = contenido + "\nProcesado"

            print(f"Subiendo el archivo: {archivo} con el nuevo contenido.")
            s3.put_object(Bucket='ttransbank', Key=archivo, Body=nuevo_contenido.encode('utf-8'))
            print(f"Archivo {archivo} subido de nuevo a S3 después de ser procesado.")
        except s3.exceptions.NoSuchKey:
            print(f"El archivo {archivo} no existe en el bucket S3.")
        except Exception as e:
            print(f"Error procesando archivo {archivo}: {str(e)}")

    with ThreadPoolExecutor() as executor:
        executor.map(procesar_archivo, mensajes)

    root, arbol = crear_arbol_binario(mensajes)
    print("Árbol AVL generado (Pre-Order):", arbol.pre_order(root))
    graficar_arbol(root)

    return {
        "desencolados": mensajes,
        "cantidad_desencolados": len(mensajes),
        "tamano_cola_restante": tamano_cola,
        "arbol_generado": arbol.pre_order(root)
    }

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="64.23.188.206", port=8090)



